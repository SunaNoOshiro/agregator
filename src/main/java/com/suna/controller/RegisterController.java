package com.suna.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.suna.model.User;
import com.suna.service.UserService;

@Controller
@RequestMapping(value = "/register")
public class RegisterController {

	@Autowired
	private UserService userService;

	@ModelAttribute("user")
	public User construct() {
		return new User();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showRegister(Model model) {
		return "register";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String register(Model model,	@Valid @ModelAttribute("user") User user, BindingResult result) {
		if (result.hasErrors()) {
			return "register";
		}
		userService.save(user);
		return "redirect:/login";
	}
	
	@ResponseBody
	@RequestMapping(value="/available/login",method = RequestMethod.POST)
	public String availableLogin(@RequestParam String login){
		Boolean available = userService.find(login) == null;
		return available.toString();
	}
	
	@ResponseBody
	@RequestMapping(value="/available/email",method = RequestMethod.POST)
	public String availableEmail(@RequestParam String email){
		Boolean available = userService.findByEmail(email) == null;
		return available.toString();
	}
}
