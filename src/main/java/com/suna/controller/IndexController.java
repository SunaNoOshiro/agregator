package com.suna.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	@RequestMapping("/index")
	public String index(){
		return "home";
	}
	
	@RequestMapping("/")
	public String home(){
		return "home";
	}
}
