package com.suna.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.suna.model.User;
import com.suna.service.UserService;

@Controller
@RequestMapping(value = "/admin")
public class AdminController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String getUsers(Model model) {
		model.addAttribute("users", userService.findAll());
		return "users";
	}

	@RequestMapping(value = "/user/info/{id}", method = RequestMethod.GET)
	public String getUser(Model model, @PathVariable Long id) {
		model.addAttribute("user", userService.findWithBlogs(id));
		return "user-detail";
	}

	@RequestMapping(value = "/user/remove/{id}", method = RequestMethod.GET)
	public String removeUser(Model model, @PathVariable Long id) {
		User user = userService.find(id);
		userService.delete(user);
		return "redirect:/admin/users";
	}
}
