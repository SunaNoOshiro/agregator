package com.suna.controller;

import java.security.Principal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.suna.model.Blog;
import com.suna.model.Item;
import com.suna.service.BlogService;
import com.suna.service.ItemService;
import com.suna.service.UserService;

@Controller
@RequestMapping(value = "/account")
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private BlogService blogService;

	@Autowired
	private ItemService itemService;

	@ModelAttribute("blog")
	public Blog constructBlog() {
		return new Blog();
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showAccount(Model model, Principal principal) {
		model.addAttribute("user", userService.findWithBlogs(principal.getName()));		
		return "account";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String createBlog(Model model, @Valid @ModelAttribute("blog") Blog blog, Principal principal, BindingResult result) {
		if (result.hasErrors()) {
			return showAccount(model, principal);
		}
		blogService.save(blog, userService.find(principal.getName()));
		return "redirect:/account";
	}

	@RequestMapping(value = "/blog/remove/{id}", method = RequestMethod.GET)
	public String removeBlog(Model model, @PathVariable Long id, Principal principal) {
		Blog blog = blogService.find(id);
		blogService.delete(blog);
		return "redirect:/account";
	}

	@RequestMapping(value = "/blog/{blogId}/item/remove/{itemId}", method = RequestMethod.GET)
	public String removeItem(Model model, @PathVariable Long blogId, @PathVariable Long itemId, Principal principal) {
		Item item = itemService.find(itemId);
		itemService.delete(item);
		return "redirect:/account";
	}

}
