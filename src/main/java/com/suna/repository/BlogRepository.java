package com.suna.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.model.Blog;
import com.suna.model.User;

public interface BlogRepository extends JpaRepository<Blog, Long> {
	List<Blog> findByUser(User user);

}
