package com.suna.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.model.Blog;
import com.suna.model.Item;

public interface ItemRepository extends JpaRepository<Item, Long>{
	List<Item> findByBlog(Blog blog, Pageable pageable);
	
	List<Item> findByBlog(Blog blog);
	
	Item findByBlogAndLink(Blog blog, String link);
}
