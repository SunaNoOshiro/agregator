package com.suna.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.suna.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

}
