package com.suna.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.suna.annotation.UniqueLogin;
import com.suna.repository.UserRepository;

public class UniqueLoginValidator implements
		ConstraintValidator<UniqueLogin, String> {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void initialize(UniqueLogin constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (userRepository == null)
			return true;
		return userRepository.findByLogin(value) == null;
	}

}
