package com.suna.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.suna.annotation.UniqueEmail;
import com.suna.repository.UserRepository;

public class UniqueEmailValidator implements
		ConstraintValidator<UniqueEmail, String> {

	@Autowired
	private UserRepository userRepository;

	@Override
	public void initialize(UniqueEmail constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (userRepository == null)
			return true;
		return userRepository.findByEmail(value) == null;
	}

}
