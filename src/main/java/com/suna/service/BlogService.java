package com.suna.service;

import java.util.List;

import com.suna.model.Blog;
import com.suna.model.User;

public interface BlogService {

	Blog find(Long id);
	
	Blog findWithItems(Long id);
	
	Blog save(Blog blog);
	
	List<Blog> findAll();

	Blog save(Blog blog, User user);
	
	void delete(Blog blog);
	
}
