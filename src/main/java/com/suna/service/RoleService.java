package com.suna.service;

import java.util.List;

import com.suna.model.Role;

public interface RoleService {

	Role find(Long id);
	
	List<Role> findAll();
}
