package com.suna.service;

import java.util.List;

import com.suna.model.Item;

public interface ItemService {

	Item find(Long id);
	
	Item save(Item item);
	
	List<Item> findAll();

	void delete(Item item);
}
