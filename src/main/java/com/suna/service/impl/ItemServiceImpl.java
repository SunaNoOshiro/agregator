package com.suna.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.suna.model.Item;
import com.suna.repository.ItemRepository;
import com.suna.service.ItemService;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemRepository itemReposiroty;

	@Override
	public Item find(Long id) {
		return itemReposiroty.findOne(id);
	}

	@Override
	public Item save(Item item) {
		return itemReposiroty.save(item);
	}

	@Override
	public List<Item> findAll() {
		return itemReposiroty.findAll();
	}

	@PreAuthorize("#item.blog.user.login == authentication.name or hasRole('ROLE_ADMIN')")
	@Override
	public void delete(@P("item") Item item) {
		itemReposiroty.delete(item);
	}
	
}
