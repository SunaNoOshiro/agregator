package com.suna.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.suna.model.Blog;
import com.suna.model.Item;
import com.suna.model.Role;
import com.suna.model.User;
import com.suna.repository.BlogRepository;
import com.suna.repository.ItemRepository;
import com.suna.repository.RoleRepository;
import com.suna.repository.UserRepository;
import com.suna.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BlogRepository blogRepository;

	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private RoleRepository roleRepository;

	@Override
	public User save(User user) {
		List<Role> roles = new ArrayList<Role>();
		roles.add(roleRepository.findOne(2L));
		
		user.setRoles(roles);
		user.setEnabled(true);
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		
		return userRepository.save(user);
	}

	@Override
	public User find(Long id) {
		return userRepository.findOne(id);
	}
	
	@Override
	public User find(String login) {
		return userRepository.findByLogin(login);
	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}
	
	@Transactional
	@Override
	public User findWithBlogs(Long id) {
		if (id == null || id <= 0)
			return null;
		User user = userRepository.findOne(id);
		List<Blog> blogs = blogRepository.findByUser(user);
		for (Blog blog : blogs) {
			List<Item> items = itemRepository.findByBlog(blog, new PageRequest(0, 10, Direction.DESC, "publishedDate"));
			blog.setItems(items);
		}
		user.setBlogs(blogs);
		return user;
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}
	
	@Override
	public User findWithBlogs(String login) {
		User user = userRepository.findByLogin(login);
		return findWithBlogs(user.getId());
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@Override
	public void delete(User user) {
		userRepository.delete(user);
		
	}

}
