package com.suna.service.impl;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;

import org.springframework.stereotype.Service;

import com.suna.model.Item;
import com.suna.rss.ObjectFactory;
import com.suna.rss.TRss;
import com.suna.rss.TRssChannel;
import com.suna.rss.TRssItem;
import com.suna.rss.exception.RssException;
import com.suna.service.RssService;

@Service
public class RssServiceImpl implements RssService {

	@Override
	public List<Item> getItems(File file) throws RssException {
		return getItems(new StreamSource(file));
	}

	@Override
	public List<Item> getItems(String url) throws RssException {
		return getItems(new StreamSource(url));

	}

	private List<Item> getItems(Source source) throws RssException {
		List<Item> items = new ArrayList<Item>();
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(ObjectFactory.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			JAXBElement<TRss> jaxbElement = unmarshaller.unmarshal(source,
					TRss.class);
			TRss rss = jaxbElement.getValue();
			List<TRssChannel> chennels = rss.getChannel();
			for (TRssChannel channel : chennels) {
				List<TRssItem> rssItems = channel.getItem();
				for (TRssItem rssItem : rssItems) {
					Item item = new Item();
					item.setTitle(rssItem.getTitle());
					item.setDescription(rssItem.getDescription());
					item.setLink(rssItem.getLink());
					item.setPublishedDate(new SimpleDateFormat(
							"EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
							.parse(rssItem.getPubDate()));
					items.add(item);
				}
			}
		} catch (JAXBException | ParseException e) {
			throw new RssException(e);
		}

		return items;
	}
}
