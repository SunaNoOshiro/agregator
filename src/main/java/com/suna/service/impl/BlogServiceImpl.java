package com.suna.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.suna.model.Blog;
import com.suna.model.Item;
import com.suna.model.User;
import com.suna.repository.BlogRepository;
import com.suna.repository.ItemRepository;
import com.suna.rss.exception.RssException;
import com.suna.service.BlogService;
import com.suna.service.RssService;

@Service
public class BlogServiceImpl implements BlogService {

	@Autowired
	private BlogRepository blogRepository;

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private RssService rssService;

	@Override
	public Blog find(Long id) {
		return blogRepository.findOne(id);
	}

	@Override
	public Blog save(Blog blog) {
		return blogRepository.save(blog);
	}

	private void saveItems(Blog blog) {		
		try {
			List<Item> items = rssService.getItems(blog.getUrl());
			for (Item item : items) {
				Item savedItem = itemRepository.findByBlogAndLink(blog,	item.getLink());
				if (savedItem == null) {
					item.setBlog(blog);
					itemRepository.save(item);
				}
			}

		} catch (RssException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<Blog> findAll() {
		return blogRepository.findAll();
	}

	@Override
	public Blog save(Blog blog, User user) {
		blog.setUser(user);
		blogRepository.save(blog);
		saveItems(blog);
		return blog;
	}

	@Override
	@PreAuthorize("#blog.user.login == authentication.name or hasRole('ROLE_ADMIN')")
	public void delete(@P("blog") Blog blog) {
		blogRepository.delete(blog);
	}

	@Override
	public Blog findWithItems(Long id) {
		Blog blog = blogRepository.findOne(id);
		List<Item> items = itemRepository.findByBlog(blog);
		blog.setItems(items);
		return blog;
	}

}
