package com.suna.service;

import java.util.List;

import com.suna.model.User;

public interface UserService {
	User save(User user);

	User find(Long id);

	List<User> findAll();

	User findWithBlogs(Long id);

	User findWithBlogs(String login);

	User find(String login);

	void delete(User user);

	User findByEmail(String email);
}
