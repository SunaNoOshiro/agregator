package com.suna.service;

import java.io.File;
import java.util.List;

import org.springframework.stereotype.Service;

import com.suna.model.Item;
import com.suna.rss.exception.RssException;

public interface RssService {

	List<Item> getItems(String url) throws RssException;

	List<Item> getItems(File file) throws RssException;

}
