package com.suna.rss.exception;

public class RssException extends Exception {

	private static final long serialVersionUID = 6438730103646045852L;

	public RssException(Throwable cause) {
		super(cause);
	}

}
