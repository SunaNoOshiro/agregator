<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras"
	prefix="tiles-x"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<tiles:insertAttribute name="css" />
<title><tiles:getAsString name="title" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
	<tiles-x:useAttribute name="current" />
	<nav>
	<div class="nav-wrapper">
		<a href="/" class="brand-logo">Logo</a>
		<ul id="nav-mobile" class="right hide-on-med-and-down">
			<li class="${current == 'home' ? 'active': ''}"><a href="/">Home</a></li>
			<security:authorize access="hasRole('ADMIN')">
				<li class="${current == 'users' ? 'active': ''}"><a
					href="/admin/users">Users</a></li>
			</security:authorize>
			<security:authorize access="! isAuthenticated()">
				<li class="${current == 'register' ? 'active': ''}"><a
					href="/register">Registration</a></li>
				<li class="${current == 'login' ? 'active': ''}"><a
					href="/login">Login</a></li>
			</security:authorize>

			<security:authorize access="isAuthenticated()">
			<li class="${current == 'account' ? 'active': ''}"><a
					href="/account">My account</a></li>
				<li><a href="/logout">Logout</a></li>
			</security:authorize>


		</ul>
	</div>
	</nav>

	<center>
		<tiles:insertAttribute name="body" />
	</center>
	<br />
	<br />
	<center>
		<tiles:insertAttribute name="footer" />
	</center>

	<tiles:insertAttribute name="js" />
</body>
</html>