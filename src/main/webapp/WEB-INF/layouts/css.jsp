<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url value="/resources/css/main.css" var="mainCss" />
<spring:url value="/resources/css/materialize.css" var="materializeCss" />
<spring:url value="/resources/css/materialize.min.css" var="materializeMinCss" />

<link href="${mainCss}" rel="stylesheet" />
<link href="${materializeCss}" rel="stylesheet" />
<link href="${materializeMinCss}" rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<spring:url value="/resources/js/jquery-2.1.1.min.js" var="jquery" />
<spring:url value="/resources/js/jquery.validate-1.14.0.min.js"	var="validate" />
<spring:url value="/resources/js/materialize.min.js" var="materialize" />

<script src="${jquery}"></script>
<script src="${materialize}"></script>
<script src="${validate}"></script>

