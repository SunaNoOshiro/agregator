<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="security"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<security:authorize access="isAuthenticated()">

	<div class="row">
		<div id="remove_blog_modal"
			class="modal bottom-sheet col s12 m6 l4 offset-m3 offset-l4">

			<div class="modal-content">
				<h4>Did you really want to delete the blog?</h4>
			</div>
			<div class="modal-footer">
				<a id="remove_blog_url"
					class="modal-action waves-effect  waves-red btn-flat" type="submit"
					style="float: right; margin: 10px;"> Delete</a>
				<button
					class="modal-action modal-close waves-effect waves-green  btn-flat"
					style="float: right; margin: 10px;" name="action">Cancel</button>
			</div>
		</div>
	</div>

	<div class="row">
		<div id="remove_item_modal"
			class="modal bottom-sheet col s12 m6 l4 offset-m3 offset-l4">

			<div class="modal-content">
				<h4>Did you really want to delete the item?</h4>
			</div>
			<div class="modal-footer">
				<a id="remove_item_url"
					class="modal-action waves-effect  waves-red btn-flat" type="submit"
					style="float: right; margin: 10px;"> Delete</a>
				<button
					class="modal-action modal-close waves-effect waves-green  btn-flat"
					style="float: right; margin: 10px;" name="action">Cancel</button>
			</div>
		</div>
	</div>

</security:authorize>


<div>
	<h1>
		<c:out value="${user.name}" />
	</h1>
</div>


<div class="row">
	<div class="col s12 m10 l10 offset-m1 offset-l1 z-depth-1">
		<ul class="tabs">
			<c:forEach var="blog" items="${user.blogs }">
				<li class="tab"><a href="#${blog.id}"><c:out
							value="${blog.name}" /></a></li>
			</c:forEach>
		</ul>
	</div>

	<c:forEach var="blog" items="${user.blogs }">
		<div id="${blog.id}" class="col s12 m10 l10 offset-m1 offset-l1">
			<div class="col s12 m6 l6 offset-m3 offset-l3">
				<h5>
					<a style="float: left;" href="<c:out value="${blog.url}" />"><c:out
							value="${blog.url}" /></a>
				</h5>
				<a style="float: right;"
					class="btn-floating waves-effect waves-light red modal-trigger"
					href="#remove_blog_modal"
					onclick="document.getElementById('remove_blog_url').href = '/account/blog/remove/${blog.id}'"><i
					class="material-icons">delete</i></a>
			</div>

			<div>
				<table class="bordered hoverable centered">
					<thead>
						<tr>
							<th data-field="title">Title</th>
							<th data-field="description">Description</th>
							<th data-field="link">Link</th>
							<th data-field="options">Options</th>
						</tr>
					</thead>

					<tbody>

						<c:forEach var="item" items="${blog.items}">
							<tr>
								<td><c:out value="${item.title}" /></td>
								<td><c:out value="${item.description}" /></td>
								<td><a href="<c:out value="${item.link}"/>"><c:out
											value="${item.link}" /></a></td>
								<td><a
									class="btn-floating waves-effect waves-light red modal-trigger"
									href="#remove_item_modal"
									onclick="document.getElementById('remove_item_url').href = '/account/blog/${blog.id}/item/remove/${item.id}'"><i
										class="material-icons">delete</i></a></td>
							</tr>
						</c:forEach>

					</tbody>
				</table>

			</div>
		</div>
	</c:forEach>
</div>

