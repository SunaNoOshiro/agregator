<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>




<div>
	<table class="bordered hoverable centered">
		<thead>
			<tr>
				<th data-field="user">User</th>
				<th data-field="options">Options</th>
			</tr>
		</thead>

		<tbody>

			<c:forEach var="user" items="${users}">
				<tr>
					<td><a href="/admin/user/info/${user.id }"><c:out value="${user.name}" />
							<c:out value="${user.login}" /></a></td>

					<td><a class="btn-floating waves-effect waves-light red"
						href="/admin/user/remove/${user.id}"><i
							class="material-icons">delete</i></a></td>
				</tr>
			</c:forEach>

		</tbody>
	</table>
</div>


