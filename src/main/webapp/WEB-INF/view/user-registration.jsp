<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<div class="row">
	<form:form commandName="user"
		cssClass="col s12 m6 l4 offset-m3 offset-l4 registration-form"
		method="post">
		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">account_circle</i>
			<form:input id="login" path="login" />
			<form:errors path="login" cssClass="red-text text-darken-4" />
			<label for="login">Login</label>
		</div>

		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">account_circle</i>
			<form:input id="name" path="name" />
			<form:errors path="name" cssClass="red-text text-darken-4" />
			<label for="name">Name</label>
		</div>

		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">location_on</i>
			<form:input id="location" path="location" />
			<form:errors path="location" cssClass="red-text text-darken-4" />
			<label for="location">Location</label>
		</div>

		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">phone</i>
			<form:input id="telephone" path="telephone" />
			<form:errors path="telephone" cssClass="red-text text-darken-4" />
			<label for="telephone">Telephone</label>
		</div>

		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">today</i>
			<form:input id="birthday" path="birthday" cssClass="datepicker" />
			<form:errors path="birthday" cssClass="red-text text-darken-4" />
			<label for="birthday" data-error="wrong" data-success="right">Birthday</label>
		</div>

		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">email</i>
			<form:input id="email" path="email" />
			<form:errors path="email" cssClass="red-text text-darken-4" />
			<label for="email" data-error="wrong" data-success="right">Email</label>
		</div>

		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">vpn_key</i>
			<form:password id="password" path="password" />
			<form:errors path="password" cssClass="red-text text-darken-4" />
			<label for="password">Password</label>
		</div>
		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">vpn_key</i> <input type="password"
				name="repassword" id="repassword"> <label for="password">Repeat
				Password</label>
		</div>

		<button class="btn-large waves-effect waves-light " type="submit"
			style="float: right; margin: 10px;" name="action">
			Submit <i class="material-icons">send</i>
		</button>
	</form:form>
</div>

<script>
	$(document).ready(function() {
		$(".registration-form").validate({
			errorElement : 'span',
			errorClass : 'error red-text text-darken-4 invalid',
			validClass : "valid",
			rules : {
				login : {
					required : true,
					minlength : 3,
					remote : {
						url : "/register/available/login",
						type : "post",
						data : {
							login : function() {
								return $("#login").val();
							}
						}
					}
				},
				name : {
					required : true,
					minlength : 3
				},

				location : {
					required : true,
					minlength : 3
				},
				telephone : {
					required : true,
					minlength : 3
				},
				birthday : {
					required : true
				},
				email : {
					required : true,
					email : true,
					minlength : 3,
					remote : {
						url : "/register/available/email",
						type : "post",
						data : {
							login : function() {
								return $("#email").val();
							}
						}
					}
				},
				password : {
					required : true,
					minlength : 6
				},
				repassword : {
					required : true,
					minlength : 6,
					equalTo : "#password"
				}
			},
			messages : {
				login : {
					remote : "User with such login already exist"
				},
				email : {
					remote : "User with such email already exist"
				}
			}
		});
	});
</script>