<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class="row">
	<form  class="col s12 m6 l4 offset-m3 offset-l4" role="form" action="/login" method="post">
		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">account_circle</i> 
			<input	id="icon_prefix" type="text" class="validate"  name="username"/> 
			<label	for="icon_prefix">Login</label>
		</div>		

		<div class="input-field col s12 m12 l12">
			<i class="material-icons prefix">vpn_key</i> 
			<input id="password" type="password" class="validate" name="password"/> 
			<label for="password">Password</label>
		</div>

		<button class="btn-large waves-effect waves-light " type="submit" style="float:right; margin:10px;"
			name="action">
			Submit <i class="material-icons">send</i>
		</button>
	</form>
</div>