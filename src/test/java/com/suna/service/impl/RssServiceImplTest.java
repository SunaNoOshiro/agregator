package com.suna.service.impl;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.suna.model.Item;
import com.suna.rss.exception.RssException;

public class RssServiceImplTest {

	RssServiceImpl rssServiceImpl;
	
	@Before
	public void setUp() throws Exception {
		rssServiceImpl = new RssServiceImpl();
	}

	@Test
	public void testGetItemsFile() throws RssException {
		List<Item> items = rssServiceImpl.getItems(new File("test-rss/javavids.xml"));
		assertNotNull(items);
		assertEquals(10, items.size());
	}

	@Test
	public void testGetItemsString() {
	}

}
